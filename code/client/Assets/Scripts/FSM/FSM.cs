using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace Core
{
    public class FSM<T>
    {
        #region Fields
        private T Owner;
		private State<T> currentState;
		private State<T> previousState;

		private State<T> previousGlobalState;
		private State<T> globalState;
		
		
		private List<State<T>> effectStates;
        #endregion Fields

        #region Properties
		public State <T>CurrentState
        {
            get { return currentState; }
            set { currentState = value; }
        }

		public State<T> PreviousState
        {
            get { return previousState; }
            set { previousState = value; }
        }
		
		public List<State<T>> EffectStates
        {
            get { return effectStates; }
            set { effectStates = value; }
        }
        #endregion Properties

        #region Constructor
        public FSM(T owner)
        {
            Owner = owner;
			effectStates = new List<State<T>>();
        }
		
		~FSM()
		{
			currentState = null;
			previousState = null;
			globalState = null;
			previousGlobalState = null;	
			effectStates.Clear();
			effectStates = null;
		}
        #endregion Constructor

        #region Methods
        public void Update()
        {
            if (globalState != null)
                globalState.Update();
            if (currentState != null)
                currentState.Update();
			if(effectStates.Count != 0)
			{
				List<State<T>> tempList = new List<State<T>>();
				tempList.AddRange(effectStates);
				foreach(State<T> state in tempList)
					state.Update();
			}
        }

		public void ChangeState(State<T> newState)
        {
            previousState = currentState;
            newState.Entity = Owner;
            newState.StateMachine = this;
            if (currentState != null)
                currentState.End();
            currentState = newState;
            if (currentState != null)
                currentState.Begin();
        }


		public void ChangeGlobalState(State<T> newState)
        {
            previousGlobalState = globalState;
            newState.Entity = Owner;
            newState.StateMachine = this;
            if (previousGlobalState != null)
                previousGlobalState.End();
            globalState = newState;
            if (globalState != null)
                globalState.Begin();
        }
		
		public void AddEffect(State<T> newState)
		{
			newState.Entity = Owner;
			newState.StateMachine = this;
			newState.Begin();
			EffectStates.Add(newState);
			
		}
		
		public void RemoveAllEffects()
		{
			List<State<T>> tempList = new List<State<T>>();
			tempList.AddRange(effectStates);
			foreach(State<T> state in tempList)
			{
					RemoveEffect(state);
			}
		}
		
		public void RemoveEffect(State<T> state)
		{
			state.End();
			EffectStates.Remove(state);
		}
		
		public void RemoveEffectsOfType(System.Type type)
		{
			List<State<T>> tempList = new List<State<T>>();
			tempList.AddRange(effectStates);
			foreach(State<T> state in tempList)
			{
				if(state.GetType() == type)
					RemoveEffect(state);
			}
		}
		
		public bool HasEffectOfType(System.Type type)
		{
			foreach(State<T> state in effectStates)
			{
				if(state.GetType() == type)
					return true;
			}
			return false;
		}
		
        public void RevertToPreviousState()
        {
            if (previousState != null)
            {
                ChangeState(previousState);
            }
        }
        #endregion Methods
    }
}