using UnityEngine;
using System.Collections;

namespace Core
{
    abstract public class State<T>
    {
        #region Fields
        private T entity;
		private FSM<T> stateMachine;
        #endregion Fields

        #region Properties
        public virtual T Entity
        {
            get { return entity; }
            set { entity = value; }
        }
		public FSM<T> StateMachine
        {
            get { return stateMachine; }
            set { stateMachine = value; }
        }
        #endregion Properties
		
		#region Constructor
		~State()
		{
			stateMachine = null;
			entity = default(T);
		}
		#endregion Constructor
		
        #region Methods
        public virtual void Begin() { }
        public virtual void Update() { }
        public virtual void End() { }
        #endregion Methods
    }
    
}