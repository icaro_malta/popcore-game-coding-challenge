﻿using Dest.Math;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntersectionHelper
{
    public static bool FindLine2Circle2(ref Line2 line, ref Circle2 circle, out Line2Circle2Intr info)
    {
        float dx, dy, A, B, C, det, t;

        dx = line.Direction.x;
        dy = line.Direction.y;

        A = dx * dx + dy * dy;
        B = 2 * (dx * (line.Center.x - circle.Center.x) + dy * (line.Center.y - circle.Center.y));
        C = (line.Center.x - circle.Center.x) * (line.Center.x - circle.Center.x) +
            (line.Center.y - circle.Center.y) * (line.Center.y - circle.Center.y) -
            circle.Radius * circle.Radius;

        det = B * B - 4 * A * C;
        if ((A <= 0.0000001) || (det < 0))
        {
            // No real solutions.
            info.Point0 = new Vector2(float.NaN, float.NaN);
            info.Point1 = new Vector2(float.NaN, float.NaN);
            info.IntersectionType = IntersectionTypes.Empty;
            return false;
        }
        else if (det == 0)
        {
            // One solution.
            t = -B / (2 * A);
            info.Point0 = new Vector2(line.Center.x + t * dx, line.Center.y + t * dy);
            info.Point1 = new Vector2(float.NaN, float.NaN);
            info.IntersectionType = IntersectionTypes.Point;
            return true;
        }
        else
        {
            // Two solutions.
            t = (float)((-B + Mathf.Sqrt(det)) / (2 * A));
            info.Point0 =
                new Vector2(line.Center.x + t * dx, line.Center.y + t * dy);
            t = (float)((-B - Mathf.Sqrt(det)) / (2 * A));
            info.Point1 =
                new Vector2(line.Center.x + t * dx, line.Center.y + t * dy);

            if(Vector3.Distance(info.Point0, line.Center) > Vector3.Distance(info.Point1, line.Center))
            {
                Vector3 temp = info.Point0;
                info.Point0 = info.Point1;
                info.Point1 = temp;
            }
            info.IntersectionType = IntersectionTypes.Point;
            return true;
        }
    }
}
