﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Game
{
    public class BubbleSlot
    {
        #region Fields
        private Bubble bubble;
        private Vector3 position;
        #endregion Fields

        #region Properties
        public Bubble Bubble { get => bubble; set => bubble = value; }
        public Vector3 Position { get => position; set => position = value; }
        #endregion Properties

        #region Constructor
        public BubbleSlot()
        {

        }
        #endregion Constructor
    }
}