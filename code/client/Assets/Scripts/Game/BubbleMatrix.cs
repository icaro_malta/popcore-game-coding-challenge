﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game
{
    public enum LineAlignment
    {
        Left,
        Right
    }

    public class BubbleLine
    {
        #region Fields
        private BubbleSlot[] bubbles;
        private LineAlignment alignment;
        private int height;
        #endregion Fields

        #region Properties
        public LineAlignment Alignment { get => alignment; set => alignment = value; }
        public BubbleSlot[] BubbleSlots { get => bubbles; set => bubbles = value; }
        public int Height { get => height; set => height = value; }
        #endregion Properties

        #region Constructor
        public BubbleLine(int size, int height, LineAlignment alignment)
        {
            this.alignment = alignment;
            this.Height = height;
            this.bubbles = new BubbleSlot[size];
        }

        #endregion Contructor
    }

    public class MatchInfo
    {
        #region Fields
        private BubbleSlot target;
        private List<BubbleSlot> match;
        #endregion Fields

        #region Properties
        public BubbleSlot Target { get => target; set => target = value; }
        public List<BubbleSlot> Match { get => match; set => match = value; }
        #endregion Properties

        #region Constructor
        public MatchInfo(BubbleSlot target, List<BubbleSlot> match)
        {
            this.target = target;
            this.match = match;
        }
        #endregion Constructor
    }

    [System.Serializable]
    public struct VisibleLines
    {
        public int min, max, target, start;
    }

    [System.Serializable]
    public class BubbleMatrix
    {
        #region Parameters
        [SerializeField]
        private int bubblesPerLine;
        [SerializeField]
        private GameObject bubblePrefab;

        [SerializeField]
        private VisibleLines visibleLines;
        #endregion Parameters


        #region Fields
        private List<BubbleLine> level;
        private Transform gameTransform;
        #endregion Fields

        #region Properties
        public Transform GameTransform { get => gameTransform; set => gameTransform = value; }
        public VisibleLines VisibleLines { get => visibleLines; set => visibleLines = value; }
        public List<BubbleLine> Level { get => level; set => level = value; }
        #endregion Properties

        #region Constructor
        public BubbleMatrix()
        {
            this.level = new List<BubbleLine>();
        }
        #endregion Constructor

        #region Methods
        public void StartGame()
        {
            GenerateLine(false, false, 2);
            for (int i = 0; i < visibleLines.start; i++)
                GenerateLine(true);

            CheckNewLine();
        }

        public void GenerateLine(bool instantiateBubbles = true, bool addOnFirstLine = false, int forceHeight = 5)
        {
            LineAlignment currentAlignment = LineAlignment.Left;
            int height = forceHeight;
            if(this.level.Count > 0)
            {
                int referenceIndex = this.level.Count - 1;
                if (addOnFirstLine)
                    referenceIndex = 0;

                currentAlignment = this.level[referenceIndex].Alignment;
                currentAlignment = (currentAlignment == LineAlignment.Left) ? LineAlignment.Right : LineAlignment.Left;

                height = this.level[referenceIndex].Height+1;
                if (addOnFirstLine)
                    height = this.level[referenceIndex].Height - 1;
            }
            float displacement = -0.25f;
            if (currentAlignment == LineAlignment.Right)
                displacement = -displacement;

            BubbleLine line = new BubbleLine(bubblesPerLine, 2, currentAlignment);
            for (int i = 0; i < bubblesPerLine; i++)
            {
                BubbleSlot slot = new BubbleSlot();
                slot.Position = new Vector3((i - bubblesPerLine / 2f) + 0.5f + displacement, height * 0.9f, 0);
                
                if(instantiateBubbles)
                {
                    Bubble bubble = GameObject.Instantiate(bubblePrefab, slot.Position, Quaternion.identity, GameTransform).GetComponent<Bubble>();
                    bubble.transform.localScale = Vector3.one;
                    bubble.Exp = UnityEngine.Random.Range(2, 9);

                    slot.Bubble = bubble;
                }


                line.BubbleSlots[i] = slot;
            }
            line.Height = height;

            if (addOnFirstLine)
                this.level.Insert(0, line);
            else
                this.level.Add(line);
        }

        public void DestroyTopLine()
        {
            BubbleLine topLine = this.level[this.level.Count - 1];

            foreach(BubbleSlot slot in topLine.BubbleSlots)
            {
                if(slot.Bubble != null)
                    GameObject.Destroy(slot.Bubble.gameObject);
            }

            this.level.Remove(topLine);
        }
            

        public MatchInfo CheckMatch(BubbleSlot newBubble)
        {
            List<BubbleSlot> matches = GetMatching(newBubble);
            if (matches.Count > 1)
            {
                int targetExp = newBubble.Bubble.Exp + (matches.Count - 1);
                BubbleSlot select = CheckBestBubblePlacement(matches, targetExp);
                select.Bubble.Exp = targetExp;

                return new MatchInfo(select, matches);
            }

            return null;
        }

        public BubbleSlot CheckBestBubblePlacement(List<BubbleSlot> possibleSlots, int exp)
        {
            int bestPlaceScore = int.MinValue;
            BubbleSlot bestSlot = possibleSlots[0];

            foreach (BubbleSlot slot in possibleSlots)
            {
                int score = GetMatching(slot, exp).Count;
                if (
                    (score > bestPlaceScore || (score == bestPlaceScore && slot.Position.y > bestSlot.Position.y))
                    &&
                    (GetSlotLineIndex(slot) == level.Count-1 || GetAboveNeighborhood(slot).Count > 0)
                   )
                {
                    bestSlot = slot;
                    bestPlaceScore = score;
                }
            }

            return bestSlot;
        }

        public List<BubbleSlot> GetConnectedGroups()
        {
            List<BubbleSlot> connectedSlots = new List<BubbleSlot>();

            Queue<BubbleSlot> queue = new Queue<BubbleSlot>();
            foreach (BubbleSlot slot in this.level[this.level.Count - 1].BubbleSlots)
            {
                if (slot.Bubble != null)
                {
                    queue.Enqueue(slot);
                    connectedSlots.Add(slot);
                }
            }

            while(queue.Count() > 0)
            {
                BubbleSlot current = queue.Dequeue();


                foreach (BubbleSlot neighbor in GetNeighborhood(current))
                {
                    if(neighbor.Bubble != null && !connectedSlots.Contains(neighbor))
                    {
                        queue.Enqueue(neighbor);
                        connectedSlots.Add(neighbor);
                    }
                }
            }

            return connectedSlots;
        }

        public List<BubbleSlot> GetDisconnectedGroups()
        {
            List<BubbleSlot> connectedGroups = GetConnectedGroups();
            return this.GetAllBubblesSlotsWithBubbles().Where(o => !connectedGroups.Contains(o)).ToList();
        }

        public void CheckNewLine()
        {
            while (level.Count > 0 && level[0].BubbleSlots.Count(o => o.Bubble != null) == 0)
                level.RemoveAt(0);

            if (level.Count == 0 || level[0].BubbleSlots.Count(o => o.Bubble != null) != 0)
                GenerateLine(false, true);
        }

        public List<BubbleSlot> GetAllBubblesSlotsWithBubbles()
        {
            List<BubbleSlot> bubbles = new List<BubbleSlot>();
            foreach (BubbleLine line in level)
            {
                for (int i = 0; i < line.BubbleSlots.Length; i++)
                {
                    if (line.BubbleSlots[i].Bubble != null)
                        bubbles.Add(line.BubbleSlots[i]);
                }
            }
            return bubbles;
        }

        public List<Bubble> GetAllBubbles()
        {
            return GetAllBubblesSlotsWithBubbles().Select(o => o.Bubble).ToList();
        }

        public BubbleSlot GetClosestBubbleSlot(Vector3 position)
        {
            BubbleSlot slot = null;
            float minDistance = float.MaxValue;

            foreach (BubbleLine line in level)
            {
                for (int i = 0; i < line.BubbleSlots.Length; i++)
                {
                    if(line.BubbleSlots[i].Bubble == null)
                    {
                        float distance = Vector3.Distance(line.BubbleSlots[i].Position, position);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            slot = line.BubbleSlots[i];
                        }
                    }
                }
            }
            return slot;
        }

        public List<BubbleSlot> GetNeighborhood(BubbleSlot slot)
        {
            List<BubbleSlot> neighborhood = new List<BubbleSlot>();


            int lineIndex = GetSlotLineIndex(slot);
            BubbleLine line = level[lineIndex];
            int slotIndex = Array.IndexOf(line.BubbleSlots, slot);

            int[] n_x = new int[] { 1, -1, 0, 1, 0, 1 };
            if (line.Alignment == LineAlignment.Left)
                n_x = new int[] { 1, -1, 0, -1, 0, -1 };
            int[] n_y = new int[] { 0, 0, 1, 1, -1, -1 };

            for (int i = 0; i < n_x.Length; i++)
            {
                int nline = lineIndex + n_y[i];
                int sindex = slotIndex + n_x[i];

                if (nline >= 0 && nline < this.level.Count)
                {
                    if (sindex >= 0 && sindex < line.BubbleSlots.Length)
                    {
                        neighborhood.Add(this.level[nline].BubbleSlots[sindex]);
                    }
                }
            }
            return neighborhood;
        }

        public int CountFilledLines()
        {
            return this.level.Where(o => o.BubbleSlots.Where(b => b.Bubble != null).Count() != 0).Count();
        }
        #endregion Methods

        #region Auxiliar Methods
        private int GetSlotLineIndex(BubbleSlot slot)
        {
            BubbleLine line = this.level.Find(o => o.BubbleSlots.Contains(slot));
            int lineIndex = this.level.IndexOf(line);
            return lineIndex;
        }

        private List<BubbleSlot> GetAboveNeighborhood(BubbleSlot slot)
        {
            return GetNeighborhood(slot).Where(o => o.Bubble != null && o.Position.y > slot.Position.y+0.3f).ToList();
        }

        private List<BubbleSlot> GetBelowNeighborhood(BubbleSlot slot)
        {
            return GetNeighborhood(slot).Where(o => o.Bubble != null && o.Position.y < slot.Position.y - 0.3f).ToList();
        }

        private List<BubbleSlot> GetMatchingNeighborhood(BubbleSlot slot)
        {
            return GetNeighborhood(slot).Where(o => o.Bubble != null && o.Bubble.Exp == slot.Bubble.Exp).ToList();
        }

        private List<BubbleSlot> GetMatchingNeighborhood(BubbleSlot slot, int exp)
        {
            return GetNeighborhood(slot).Where(o => o.Bubble != null && o.Bubble.Exp == exp).ToList();
        }

        private List<BubbleSlot> GetMatching(BubbleSlot slot)
        {
            return GetMatching(slot, slot.Bubble.Exp);
        }

        private List<BubbleSlot> GetMatching(BubbleSlot targetSlot, int exp)
        {
            List<BubbleSlot> matchingNodes = new List<BubbleSlot>();

            Queue<BubbleSlot> nodesToEvaluate = new Queue<BubbleSlot>();

            nodesToEvaluate.Enqueue(targetSlot);
            matchingNodes.Add(targetSlot);

            while (nodesToEvaluate.Count > 0)
            {
                BubbleSlot current = nodesToEvaluate.Dequeue();

                List<BubbleSlot> matchingNeighborhood = GetMatchingNeighborhood(current, exp);
                matchingNeighborhood = matchingNeighborhood.Where(o => !matchingNodes.Contains(o)).ToList();

                foreach (BubbleSlot slot in matchingNeighborhood)
                {
                    matchingNodes.Add(slot);
                    nodesToEvaluate.Enqueue(slot);
                }
            }
            return matchingNodes;
        }
        #endregion Auxiliar Methods
    }
}
