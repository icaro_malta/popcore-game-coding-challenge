﻿
using Core;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{

    public class GameController : MonoBehaviour
    {
        #region Fields
        [SerializeField]
        private BubbleMatrix matrix;
        [SerializeField]
        private Grid grid;
        [SerializeField]
        private GameObject explosion;


        private Shooter shooter;
        private FSM<GameController> fsm;
        #endregion Fields

        #region Singleton
        private static GameController instance;
        public static GameController Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion Singleton

        #region Properties
        public BubbleMatrix Matrix { get => matrix; set => matrix = value; }
        public Shooter Shooter { get => shooter; set => shooter = value; }
        public Grid Grid { get => grid; set => grid = value; }
        public GameObject Explosion { get => explosion; }
        #endregion Properties

        #region Methods
        public void Awake()
        {
            GameController.instance = this;

            matrix.GameTransform = transform;
            matrix.StartGame();

            shooter = transform.Find("Shooter").GetComponent<Shooter>();
            shooter.SetBubbleMatrix(Matrix);

            fsm = new FSM<GameController>(this);
            fsm.ChangeState(new ShowingBoard());
        }

        public void Update()
        {
            fsm.Update();
        }
        #endregion Methods


        #region Events
        public void OnShotBubble(BubbleSlot target, Sequence throwingAnimation)
        {
            (fsm.CurrentState as Base).OnShotBubble(target, throwingAnimation);
        }
        #endregion Events
    }

}