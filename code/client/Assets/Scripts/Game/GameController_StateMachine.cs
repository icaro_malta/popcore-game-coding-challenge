﻿using Core;
using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class Base : State<GameController>
    {
        public virtual void OnShotBubble(BubbleSlot target, Sequence throwingAnimation)
        {
        }
    }

    public class ShowingBoard : Base
    {
        #region Methods
        public override void Begin()
        {
            this.Entity.Shooter.CanShoot = false;

            Sequence sequence = DOTween.Sequence();
            //Displaying bubble
            foreach (Bubble slot in this.Entity.Matrix.GetAllBubbles())
            {
                slot.transform.localScale = Vector3.zero;
                sequence.Insert(0.4f, slot.transform.DOScale(Vector3.one, UnityEngine.Random.Range(0.3f, 0.5f)).SetEase(Ease.OutBack).SetDelay(UnityEngine.Random.Range(0.2f, 0.4f)));
            }
            sequence.OnComplete(() => this.StateMachine.ChangeState(new Shooting()));
            sequence.Play();
        }

        public override void End()
        {
            base.End();
        }
        #endregion Methods
    }

    public class Shooting : Base
    {
        #region Methods
        public override void Begin()
        {
            base.Begin();
            this.Entity.Shooter.CanShoot = true;
        }
        public override void End()
        {
            base.End();
            this.Entity.Shooter.CanShoot = false;
        }
        #endregion Methods

        #region Event Handlers
        public override void OnShotBubble(BubbleSlot target, Sequence throwingAnimation)
        {
            StateMachine.ChangeState(new ThrowingBall(target, throwingAnimation));
        }
        #endregion Event Handlers
    }

    public class ThrowingBall : Base
    {
        #region Fields
        private BubbleSlot target;
        private Sequence throwingAnimation;
        #endregion Fields

        #region Constructor
        public ThrowingBall(BubbleSlot target, Sequence throwingAnimation)
        {
            this.target = target;
            this.throwingAnimation = throwingAnimation;
        }
        #endregion Constructor

        #region Methods
        public override void Begin()
        {
            base.Begin();
            this.throwingAnimation.Play().OnComplete(() => OnCompleteThrowingAnimation());
        }
        public override void End()
        {
            base.End();
        }
        #endregion Methods

        #region Animation
        private void OnCompleteThrowingAnimation()
        {
            StateMachine.ChangeState(new ProcessingMatch(this.target));
        }
        #endregion Animation
    }

    public class ProcessingMatch : Base
    {
        #region Fields
        private BubbleSlot target;
        private int level;
        #endregion Fields

        #region Constructor
        public ProcessingMatch(BubbleSlot target, int level = 0)
        {
            this.target = target;
            this.level = level;
        }
        #endregion Constructor

        #region Methods
        public override void Begin()
        {
            base.Begin();
            MatchInfo info = this.Entity.Matrix.CheckMatch(this.target);
            if(info != null)
            {
                Entity.StartCoroutine(MatchGems(info));
            }
            else
            {
                StateMachine.ChangeState(new MoveBubblesDown());
            }
        }
        public override void End()
        {
            base.End();
        }
        #endregion Methods

        #region Flow
        private IEnumerator MatchGems(MatchInfo info)
        {
            Vector3 matchPosition = info.Target.Position;
            int targetExp = info.Target.Bubble.Exp;

            foreach (BubbleSlot slot in info.Match)
            {
                slot.Bubble.transform.DOMove(info.Target.Bubble.transform.position, 0.15f);
            }
            yield return new WaitForSeconds(0.15f);

            foreach (BubbleSlot slot in info.Match)
            {
                if (slot != info.Target)
                {
                    GameObject.Destroy(slot.Bubble.gameObject);
                    slot.Bubble = null;
                }
            }

            float alpha = Mathf.Clamp01(info.Match.Count / 6);
            Entity.Grid.ApplyExplosiveForce(Mathf.Lerp(3, 8, alpha), matchPosition, Mathf.Lerp(3, 8, alpha));

            this.Entity.StartCoroutine(DestroyDisconnectedGroup(matchPosition));


            if (targetExp >= 11)
            {
                GameObject.Instantiate(Entity.Explosion, matchPosition, Quaternion.identity, Entity.transform);
                yield return Entity.StartCoroutine(PlayExplosion(info.Target));
                this.Entity.StartCoroutine(DestroyDisconnectedGroup(matchPosition));
                StateMachine.ChangeState(new MoveBubblesDown());
            }
            else
            {
                if(info.Target != null)
                    StateMachine.ChangeState(new ProcessingMatch(info.Target, level + 1));
                else
                    StateMachine.ChangeState(new MoveBubblesDown());
            }
        }

        private IEnumerator PlayExplosion(BubbleSlot target)
        {
            List<BubbleSlot> explosion = this.Entity.Matrix.GetNeighborhood(target);
            explosion.Add(target);

            foreach (BubbleSlot slot in explosion)
            {
                if (slot.Bubble != null)
                {
                    GameObject.Destroy(slot.Bubble.gameObject);
                    slot.Bubble = null;
                }
            }
            yield return null;
        }

        private IEnumerator DestroyDisconnectedGroup(Vector3 matchPosition)
        {
            List<BubbleSlot> slots = Entity.Matrix.GetDisconnectedGroups();

            List<Bubble> bubblesToDestroy = new List<Bubble>();

            foreach(BubbleSlot slot in slots)
            {
                Vector3 direction = matchPosition - slot.Bubble.transform.position;
                direction.x = -direction.x;
                direction.Normalize();
                direction = Vector3.Slerp(Vector3.up, direction, 0.5f);

                Rigidbody2D rigidbody = slot.Bubble.gameObject.AddComponent<Rigidbody2D>();
                rigidbody.AddForce(direction * 5, ForceMode2D.Impulse);
                slot.Bubble.transform.DOScale(Vector3.one * 1.15f, 0.2f);

                bubblesToDestroy.Add(slot.Bubble);
                slot.Bubble = null;
            }

            yield return new WaitForSeconds(2f);
            
            foreach (Bubble bubble in bubblesToDestroy)
            {
                GameObject.Destroy(bubble.gameObject);
            }
        }
        #endregion Flow
    }

    public class MoveBubblesDown : Base
    {
        #region Methods
        public override void Begin()
        {
            this.Entity.Matrix.CheckNewLine();

            if (Entity.Matrix.Level.Count-1 < Entity.Matrix.VisibleLines.target)
                MoveDownBubbles();

            while (Entity.Matrix.Level.Count-1 < Entity.Matrix.VisibleLines.min)
                MoveDownBubbles();

            while (Entity.Matrix.Level.Count-1 > Entity.Matrix.VisibleLines.max)
                MoveUpBubbles();

            StateMachine.ChangeState(new Shooting());
        }
        #endregion Methods

        #region Auxiliar Methods
        private void MoveDownBubbles()
        {
            this.Entity.Matrix.GenerateLine(true);
            foreach(BubbleLine line in this.Entity.Matrix.Level)
            {
                line.Height -= 1;
                foreach(BubbleSlot slot in line.BubbleSlots)
                {
                    slot.Position = new Vector2(slot.Position.x, line.Height * 0.9f);
                    if (slot.Bubble != null)
                        slot.Bubble.transform.DOMove(slot.Position, 0.3f);
                }
            }
        }
        private void MoveUpBubbles()
        {
            this.Entity.Matrix.DestroyTopLine();

            foreach (BubbleLine line in this.Entity.Matrix.Level)
            {
                line.Height += 1;
                foreach (BubbleSlot slot in line.BubbleSlots)
                {
                    slot.Position = new Vector2(slot.Position.x, line.Height * 0.9f);
                    if (slot.Bubble != null)
                        slot.Bubble.transform.DOMove(slot.Position, 0.3f);
                }
            }
        }
        #endregion Auxiliar Methods
    }
}
