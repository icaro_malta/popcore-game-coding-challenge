﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Game
{
    public class Bubble : MonoBehaviour
    {
        #region Unity Fields
        [SerializeField]
        private Color[] colors;
        #endregion Unity Fields

        #region Fields
        [SerializeField]
        private int exp;

        private SpriteRenderer glow;
        #endregion Fields

        #region Properties
        public int Exp
        {
            get
            {
                return this.exp;
            }
            set
            {
                this.exp = Mathf.Min(value, 12);

                this.transform.Find("Text").GetComponent<TextMeshPro>().text = ((int)Mathf.Pow(2, exp)).ToString();

                Color color = colors[this.exp - 1];
                glow.color = color;
                this.GetComponent<SpriteRenderer>().color = color;
                this.transform.Find("Text").GetComponent<TextMeshPro>().color = color;
            }
        }

        public bool Filled
        {
            get
            {
                return this.transform.Find("Filled").gameObject.active;
            }
            set
            {
                this.transform.Find("Filled").gameObject.SetActive(value);
            }
        }

        #endregion Properties

        #region Monobehaviour
        public void Awake()
        {
            glow = transform.Find("Glow").GetComponent<SpriteRenderer>();
        }

        public void Update()
        {
            float interpolation = Mathf.Repeat(Time.timeSinceLevelLoad, 4)/4;

            Vector3 midpoint = Vector3.Lerp(new Vector3(-20, -10), new Vector3(20, 10), interpolation);
            float distance = Vector3.Distance(Vector3.Project(transform.position, new Vector3(1, 1)), midpoint);

            float radius = 4;

            float alpha = (radius - distance) / radius;
            if (distance > radius)
                distance = 0;

            glow.color = new Color(glow.color.r, glow.color.g, glow.color.b, alpha);
        }
        #endregion Monobehaviour
    }
}

