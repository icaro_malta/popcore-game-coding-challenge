﻿using Dest.Math;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Game
{
    public class ShootPath
    {
        #region Fields
        private List<Vector3> path;
        private BubbleSlot bubbleTarget;
        #endregion Fields

        #region Properties
        public List<Vector3> Path { get => path; set => path = value; }
        public BubbleSlot BubbleTarget { get => bubbleTarget; set => bubbleTarget = value; }
        #endregion Properties

        #region Constructor
        public ShootPath()
        {
            this.path = new List<Vector3>();
        }
        #endregion Constructor
    }

    public class Shooter : MonoBehaviour
    {
        #region Unity Fields
        [SerializeField]
        private Bubble bubblePrefab;
        [SerializeField]
        private float texturespeed;
        #endregion Unity Fields

        #region Fields
        private Bubble center, side, target;
        private BubbleMatrix matrix;

        private LineRenderer trajectory;
        private bool canShoot;
        #endregion Fields

        #region Properties
        public bool CanShoot { get => canShoot; set => canShoot = value; }
        #endregion Properties

        #region Monobehaviours Methods
        public void Awake()
        {
            CreateNewBubble(false);
            CreateNewBubble(true);

            trajectory = transform.Find("Trajectory").GetComponent<LineRenderer>();

            target = GameObject.Instantiate(bubblePrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Bubble>();

            target.name = "Target";
            target.gameObject.SetActive(false);
        }

        public void Update()
        {
            trajectory.material.mainTextureOffset -= new Vector2(Time.deltaTime * texturespeed, 0);
            CheckBubbleTarget();
        }
        #endregion Monobehaviour Methods

        #region Methods
        public void SetBubbleMatrix(BubbleMatrix matrix)
        {
            this.matrix = matrix;
        }

        public void CreateNewBubble(bool animation = true)
        {
            center = side;
            side = GameObject.Instantiate(bubblePrefab, Vector3.zero, Quaternion.identity, transform).GetComponent<Bubble>();
            side.transform.localPosition = Vector3.left * 1.1f;

            side.GetComponent<Bubble>().Exp = Random.Range(2, 9);

            if (animation)
            {
                center.transform.DOLocalMove(Vector3.zero, 0.3f);
                center.transform.DOScale(Vector3.one, 0.3f);

                side.transform.localScale = Vector3.one * 0.1f;
                side.transform.DOScale(Vector3.one * 0.8f, 0.3f);
            }

            if(center != null)
                center.name = "Center";
            if (side != null)
                side.name = "Side";

            if(center != null)
                center.Filled = true;
            if (side != null)
                side.Filled = true;
        }

        public void CheckBubbleTarget()
        {
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));
            Vector3 direction = worldPosition - trajectory.transform.position;

            if ((Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)) && Vector3.Angle(direction, Vector3.up) < 60 && CanShoot)
            {
                trajectory.gameObject.SetActive(true);

                ShootPath shootPath = GetShootPath(trajectory.transform.position, direction);
                trajectory.positionCount = shootPath.Path.Count;
                trajectory.SetPositions(shootPath.Path.ToArray());

                this.target.transform.position = shootPath.BubbleTarget.Position;
                this.target.Exp = center.Exp;
                this.target.gameObject.SetActive(true);

                //Shoot
                if(Input.GetMouseButtonUp(0))
                {
                    Sequence sequence = DOTween.Sequence();

                    Bubble bubble = center;
                    CreateNewBubble();

                    Vector3[] path = shootPath.Path.ToArray();
                    shootPath.BubbleTarget.Bubble = bubble;
                    path[path.Length - 1] = shootPath.BubbleTarget.Position;
                    sequence.Append(bubble.transform.DOPath(path, 0.4f));
                    bubble.Filled = false;
                    bubble.transform.parent = transform.parent;

                    GameController.Instance.OnShotBubble(shootPath.BubbleTarget, sequence);
                }
            }
            else
            {
                trajectory.gameObject.SetActive(false);
                target.gameObject.SetActive(false);
            }
        }
        #endregion Methods

        #region Auxiliar Methods
        private ShootPath GetShootPath(Vector3 source, Vector3 direction, ShootPath shotPath = null)
        {
            if (shotPath == null)
            {
                shotPath = new ShootPath();
                shotPath.Path.Add(source);
            }

            Line2 trajectoryLine = new Line2(source, direction);

            Line2Circle2Intr lineCircleIntr = new Line2Circle2Intr();
            float minDistance = float.MaxValue;

            bool collide = false;
            foreach (Bubble bubble in matrix.GetAllBubbles())
            {
                Line2Circle2Intr tempResult;
                Circle2 circle = new Circle2(bubble.transform.position, 0.5f);
                if (IntersectionHelper.FindLine2Circle2(ref trajectoryLine, ref circle, out tempResult))
                {
                    collide = true;
                    float distance = Vector3.Distance(tempResult.Point0, trajectoryLine.Center);
                    if (distance < minDistance)
                    {
                        minDistance = distance;
                        lineCircleIntr = tempResult;
                    }
                }
            }

            if(collide)
            {
                shotPath.Path.Add(lineCircleIntr.Point0);
                shotPath.BubbleTarget = matrix.GetClosestBubbleSlot(lineCircleIntr.Point0);
            }
            else
            {
                Line2Line2Intr info;

                Line2 topBorder = new Line2(Vector3.up * 5, Vector3.left);

                Intersection.FindLine2Line2(ref trajectoryLine, ref topBorder, out info);
                float borderSize = 2.75f;

                if (Mathf.Abs(info.Point.x) < borderSize)
                {
                    shotPath.Path.Add(info.Point);
                    shotPath.BubbleTarget = matrix.GetClosestBubbleSlot(info.Point);
                }
                else
                {
                    Line2[] borders = new Line2[]
                    {
                            new Line2(Vector3.left*2.75f, Vector3.up),
                            new Line2(Vector3.right*2.75f, Vector3.down)
                    };

                    Vector3 sideCollisionPoint = Vector3.zero;
                    for (int i = 0; i < borders.Length; i++)
                    {
                        Intersection.FindLine2Line2(ref trajectoryLine, ref borders[i], out info);
                        if (info.Point.y > trajectory.transform.position.y && info.Point != trajectoryLine.Center)
                        {
                            sideCollisionPoint = info.Point;
                        }
                    }
                    shotPath.Path.Add(sideCollisionPoint);
                    direction.x = -direction.x;
                    GetShootPath(sideCollisionPoint, direction, shotPath);
                }
            }
            return shotPath;
        }
        #endregion Auxiliar Methods
    }
}

